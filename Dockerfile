FROM amd64/ubuntu:20.04
ARG UBUNTU_NAME=focal

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -yqq update && \
	apt-get -yqq dist-upgrade && \
	TZ=Europe/Berlin apt-get -yqq install \
	libgtk-3-0 \
	libxss1 \
	npm \
	parallel \
	plantuml \
	ruby \
	pandoc && \
	apt autoremove

RUN npm install mermaid.cli wavedrom-cli
ENV PATH ${PATH}:/node_modules/.bin

RUN gem install asciidoctor asciidoctor-pdf asciidoctor-diagram asciimath
